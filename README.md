# Universidad Nacional Autónoma de México
# Facultad de Ciencias
# Ciencias de la Computación

## Sistemas Operativos

Semestre 2021-1

![Build Status](https://gitlab.com/SistemasOperativos-Ciencias-UNAM/2021-1/tareas-so/badges/master/pipeline.svg)

En este repositorio estaremos manejando las tareas de la materia de [sistemas operativos][url-sistemas_operativos-gitlab] que se imparte en la [Facultad de Ciencias, UNAM][url-sistemas_operativos-fciencias] en el semestre 2021-1.

### Estructura de directorios

+ Cada alumno deberá tener una carpeta dentro de la que hará entrega de sus tareas y prácticas
+ Si la práctica es en equipo, uno sube la carpeta de la práctica y los demás entregan una liga simbólica a la carpeta correspondiente de la práctica.
+ Cada tarea o práctica deberá tener lo siguiente:
    * Un archivo `README.md` donde se entregará la documentación en formato _markdown_. Se puede hacer uso de un directorio `img/` para guardar las imágenes o capturas necesarias para la documentación
    * Un archivo `Makefile` que servirá para compilar el programa y hacer pruebas

```
content/tarea/AndresHernandez/
├── .gitkeep
├── README.md
├── tareas
│   ├── tarea-0/
│   │   ├── img/
│   │   │   └── ...
│   │   ├── README.md
│   │   └── ...
│   ├── tarea-1/
│   │   ├── img/
│   │   │   └── ...
│   │   ├── README.md
│   │   ├── Makefile
│   │   ├── programa.c
│   │   └── ...
│   ├── ...
│   └── tarea-n/
│   │   ├── img/
│   │   │   └── ...
│   │   ├── README.md
│   │   └── ...
└── practicas/
    ├── practica-1/
    │   ├── img/
    │   │   └── ...
    │   ├── README.md
    │   └── ...
    ├── practica-2/ -> ../../EmilioCabrera/practicas/practica-2/
    ├── ...
    └── practica-m/
        ├── img/
        │   └── ...
        ├── README.md
        └── ...
```

--------------------------------------------------------------------------------

### Flujo de trabajo para la entrega de tareas

![https://drive.google.com/open?id=1_fhDH4NX_2kvMWoRupcdpaOqAATsbw9s](content/img/000-workflow.png "")

### Acceder al repositorio principal

+ Abre la URL del [repositorio de tareas para la materia][repositorio-tareas]

|                                       |
|:-------------------------------------:|
| ![](content/img/001-Main_repo.png "") |

+ [Inicia sesión en GitLab][gitlab-login]

|                                     |
|:-----------------------------------:|
| ![](content/img/002-Sign_in.png "") |

--------------------------------------------------------------------------------

#### Hacer `fork` al repositorio principal

+ Da clic en el botón `fork` para crear una copia del repositorio `tareas-so` en tu cuenta de usuario

|                                       |
|:-------------------------------------:|
| ![](content/img/003-Fork_repo.png "") |

+ Espera a que el __fork__ se complete

|                                              |
|:--------------------------------------------:|
| ![](content/img/004-Fork_in_progress.png "") |

--------------------------------------------------------------------------------

#### Clonar el repositorio

+ Accede a la URL del repositorio `tareas-so` asociado a **tu cuenta de usuario**

```
https://gitlab.com/USUARIO/tareas-so.git
```

+ Obten la URL de tu repositorio `tareas-so` y bájalo a tu equipo con `git clone`:

|                                                       |
|:-----------------------------------------------------:|
| ![](content/img/005-Fork_successful-clone_URL.png "") |

```
$ git clone https://gitlab.com/USUARIO/tareas-so.git
Cloning into 'tareas-so'...
remote: Enumerating objects: 134, done.
remote: Counting objects: 100% (134/134), done.
remote: Compressing objects: 100% (112/112), done.
remote: Total 134 (delta 11), reused 134 (delta 11)
Receiving objects: 100% (134/134), 1017.31 KiB | 0 bytes/s, done.
Resolving deltas: 100% (11/11), done.
Checking connectivity... done.
```

+ Lista el contenido, especialmente la carpeta `content`

```
$ cd tareas-so/
$ ls -lA
total 36
drwxrwsr-x 8 tonejito users 4096 sep 28 18:30 .git
drwxrwsr-x 4 tonejito users 4096 sep 28 18:30 content
drwxrwsr-x 2 tonejito users 4096 sep 28 18:30 static
drwxrwsr-x 4 tonejito users 4096 sep 28 18:30 themes
-rw-rw-r-- 1 tonejito users    8 sep 28 18:30 .gitignore
-rw-rw-r-- 1 tonejito users  319 sep 28 18:30 .gitlab-ci.yml
-rw-rw-r-- 1 tonejito users 1236 sep 28 18:30 config.toml
-rw-rw-r-- 1 tonejito users  408 sep 28 18:30 README.md
-rw-rw-r-- 1 tonejito users 1078 sep 28 18:30 LICENSE
-rw-rw-r-- 1 tonejito users 1078 sep 28 18:30 Makefile

$ tree -a content
content
├── _index.md
├── post
│   ├── 2020-09-28-tareas.md
│   ├── .gitkeep
│   └── _index.md
└── tarea
    ├── .gitkeep
    └── _index.md
```

--------------------------------------------------------------------------------

#### Crear rama personal

+ Crea una rama con tu nombre para versionar tus cambios

```
$ git checkout -b AndresHernandez
Switched to a new branch 'AndresHernandez'
```

+ Comprueba que te encuentres en la rama con tu nombre. Debe tener el prefijo `*`

```
$ git branch
* AndresHernandez
  entregas
```

--------------------------------------------------------------------------------

#### Agregar carpeta personal

+ Accede al repositorio y crea una carpeta con tu nombre bajo la ruta `content/tarea`

```
$ mkdir -vp content/tarea/AndresHernandez/
mkdir: created directory ‘content/tarea/AndresHernandez/’
```

>>>
Dentro de esa carpeta es donde debes poner los archivos de **TODAS** las tareas que hagas
>>>

--------------------------------------------------------------------------------

##### Agregar archivo con tu nombre

+ Crea el archivo `.gitkeep` y un archivo de Markdown que contenga tu nombre

>>>
Observa el _contenido de ejemplo_ en el siguiente paso
>>>

```
$ touch  content/tarea/AndresHernandez/.gitkeep
$ editor content/tarea/AndresHernandez/README.md
```

+ Contenido de ejemplo para el archivo README.md (incluye los guiones `---` en el contenido del archivo)

```
---
title: NOMBRE COMPLETO
subtitle: NÚMERO DE CUENTA
date: 2019-08-05
comments: false
---

Esta es la carpeta de NOMBRE COMPLETO
```

--------------------------------------------------------------------------------

#### Enviar cambios al repositorio

+ Una vez que hayas creado los archivos, revisa el estado del repositorio

```
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Untracked files:
  (use "git add <file>..." to include in what will be committed)

	content/tarea/AndresHernandez/

nothing added to commit but untracked files present (use "git add" to track)
```

+ Lista el directorio y agrega los archivos con `git add`

```
$ cd content/tarea/AndresHernandez/
$ ls -lA
total 4
-rw-rw-r-- 1 tonejito users   0 sep 28 20:00 .gitkeep
-rw-rw-r-- 1 tonejito users 145 sep 28 20:00 README.md

$ git add .gitkeep README.md
```

+ Versiona los archivos con `git commit`

>>>
Usa **comillas simples** para especificar el mensaje del _commit_
>>>

```
$ git commit -m 'Carpeta de Andrés Hernández'
[master 97c54ea] Carpeta de Andrés Hernández
 2 files changed, 10 insertions(+)
 create mode 100644 content/tarea/AndresHernandez/.gitkeep
 create mode 100644 content/tarea/AndresHernandez/README.md
```

+ Revisa que el _remote_ apunte a **tu repositorio** con `git remote`

```
$ git remote -v
origin	https://gitlab.com/USUARIO/tareas-so.git (fetch)
origin	https://gitlab.com/USUARIO/tareas-so.git (push)
```

+ Revisa la rama en la que estas para enviarla a GitLab

```
$ git branch
* AndresHernandez
  entregas
```

+ Envía los cambios a **tu repositorio** utilizando `git push`

```
$ git push -u origin AndresHernandez
Username for 'https://gitlab.com': USUARIO
Password for 'https://USUARIO@gitlab.com':
Counting objects: 6, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (6/6), done.
Writing objects: 100% (6/6), 610 bytes | 610.00 KiB/s, done.
Total 6 (delta 2), reused 0 (delta 0)
remote:
remote: To create a merge request for AndresHernandez, visit:
remote:   https://gitlab.com/USUARIO/tareas-so/merge_requests/new?merge_request%5Bsource_branch%5D=AndresHernandez
remote:
To https://gitlab.com/USUARIO/tareas-so.git
 * [new branch]      AndresHernandez -> AndresHernandez
Branch 'AndresHernandez' set up to track remote branch 'AndresHernandez' from 'origin'.
```

--------------------------------------------------------------------------------

#### Crea un `merge request` para entregar tu tarea

Para integrar las tareas de todos se utilizará la funcionalidad `merge request` de GitLab

+ Accede a la url de **tu repositorio**:

```
https://gitlab.com/USUARIO/tareas-so
```

|                                            |
|:------------------------------------------:|
| ![](content/img/006-Fork-commit_ok.png "") |

+ Selecciona la rama con tu nombre

|                                                |
|:----------------------------------------------:|
| ![](content/img/007-Fork_select_branch.png "") |

+ Verifica que aparezca el título de tu commit

|                                             |
|:-------------------------------------------:|
| ![](content/img/008-Fork_new_branch.png "") |

+ Ve a la sección llamada `merge requests` en la barra lateral

+ Crea un nuevo `merge request` para enviar los cambios al repositorio central

|                                         |
|:---------------------------------------:|
| ![](content/img/009-Fork-new_MR.png "") |

--------------------------------------------------------------------------------

##### Llena los datos del _merge request_

+ Escribe un título y una descripción que sirva como vista previa para tu entrega

+ Selecciona _la rama con tu nombre_ como **origen** y la rama `tareas-so` como **destino**

+ Da clic en el botón _submit_ para crear tu _merge request_

|                                          |
|:----------------------------------------:|
| ![](content/img/010-Fork-MR_data.png "") |

--------------------------------------------------------------------------------

##### Notificaciones de creación y seguimiento del MR

+ Una vez que hayas enviado el `merge request`, le llegará un correo electrónico al responsable para que integre tus cambios

|                                             |
|:-------------------------------------------:|
| ![](content/img/011-Main_MR_created.png "") |

+ Cuando se hayan integrado tus cambios te llegará un correo electrónico de confirmación y aparecerá en el panel del [repositorio principal][repositorio-tareas]

|                                            |
|:------------------------------------------:|
| ![](content/img/012-Main_MR_merged.png "") |

--------------------------------------------------------------------------------

>>>
**Protip**
Puedes cambiar la visibilidad del proyecto a privada

+ En la barra lateral seleccionar `Settings` > `General`
+ Expande la parte llamada `Permissions`, ahí puedes seleccionar la visibilidad deseada
+ Da clic en `Save changes` para aplicar
>>>

--------------------------------------------------------------------------------

##### Vista en GitLab

+ <https://gitlab.com/SistemasOperativos-Ciencias-UNAM/2021-1/tareas-so>

##### Vista web

+ <https://SistemasOperativos-Ciencias-UNAM.gitlab.io/2021-1/tareas-so/>

--------------------------------------------------------------------------------

[url-sistemas_operativos-gitlab]: https://SistemasOperativos-Ciencias-UNAM.gitlab.io/ "Página en GitLab"
[url-sistemas_operativos-fciencias]: "http://www.fciencias.unam.mx/docencia/horarios/20211/1556/713" "Sistemas Operativos 2021-1"

[repositorio-tareas]: https://gitlab.com/SistemasOperativos-Ciencias-UNAM/2021-1/tareas-so
[gitlab-login]: https://gitlab.com/users/sign_in
[repositorio-personal]: https://gitlab.com/USUARIO/tareas-so

